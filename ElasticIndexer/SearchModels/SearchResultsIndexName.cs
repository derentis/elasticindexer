﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ElasticIndexer
{
    public class SearchResultsIndexName
    {
        public string health { get; set; }
        public string status { get; set; }
        public string index { get; set; }
        public string uuid { get; set; }
        public string pri { get; set; }
        public string rep { get; set; }
        [JsonProperty("docs.count")] public string docsCount { get; set; }
        [JsonProperty("docs.deleted")] public string docsDeleted { get; set; }
        [JsonProperty("store.size")] public string storeSize { get; set; }
        [JsonProperty("pri.store.size")] public string priStoreSize { get; set; }
    }
}
