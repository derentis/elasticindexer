﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ElasticIndexer
{
    public class ReindexingProcessor
    {
        // create index for the beat/month/domain/typegroup
        // example: metricbeat-6.3.1-2018.10.01-ramm-ctx-october

        // index structure used for all archival indices
        static string jsonIndexSettingsWarm =
          "{" +
            "\"settings\" : {" +
              "\"index\": {" +
                "\"codec\": \"best_compression\"," +
                "\"routing\": {" +
                  "\"allocation\": {" +
                    "\"exclude\": {" +
                      "\"box_type\": \"hot,cold\"" +
                    "}," +
                    "\"include\": {" +
                      "\"box_type\": \"\"" +
                    "}," +
                    "\"require\": {" +
                      "\"box_type\": \"warm\"" +
                    "}" +
                  "}" +
                "}," +
                "\"number_of_shards\": \"1\"," +
                "\"number_of_replicas\": \"0\"" +
              "}" +
            "}" +
          "}";

        static string jsonIndexSettingsCold =
            "{\"settings\" : {" +
                "\"index\": {" +
                    "\"codec\": \"best_compression\"," +
                    "\"routing\": {" +
                        "\"allocation\": {" +
                            "\"exclude\": {" +
                                "\"box_type\": \"hot,warm\"" +
                            "}," +
                            "\"include\": {" +
                                "\"box_type\": \"\"" +
                            "}," +
                            "\"require\": {" +
                                "\"box_type\": \"cold\"" +
                            "}" +
                        "}" +
                    "}," +
                    "\"number_of_shards\": \"1\"," +
                    "\"number_of_replicas\": \"0\"}" +
                "}" +
            "}";

        static string jsonIndexSettingsHot =
            "{\"settings\" : {" +
                "\"index\": {" +
                    "\"codec\": \"best_compression\"," +
                    "\"routing\": {" +
                        "\"allocation\": {" +
                            "\"exclude\": {" +
                                "\"box_type\": \"cold\"" +
                            "}," +
                            "\"include\": {" +
                                "\"box_type\": \"warm\"" +
                            "}," +
                            "\"require\": {" +
                                "\"box_type\": \"hot\"" +
                            "}" +
                        "}" +
                    "}," +
                    "\"number_of_shards\": \"1\"," +
                    "\"number_of_replicas\": \"3\"}" +
                "}" +
            "}";

        // Declare operation classes
        private static RESTGet GETClient;
        private static RESTPut PUTClient;
        private static RESTPost POSTClient;

        //Declare variables
        private static List<string> processedMonthlyIndices = new List<string>();
        private List<string> exceptionList = new List<string>();
        HttpClient _client;
        long totalDocsCount;
        public ReindexingProcessor(HttpClient client)
        {
            _client = client;
            instantiateOperations();
        }

        // encapsulate error reporting
        public List<string> getExceptionList()
        {
            return exceptionList;
        }

        public void setExceptionList(List<string> exceptionList)
        {
            this.exceptionList = exceptionList;
        }

        public void instantiateOperations()
        {
            GETClient = new RESTGet(_client, this);
            PUTClient = new RESTPut(_client, this);
            POSTClient = new RESTPost(_client, this);
        }

        // reindex indicies from month into categorized merged indices
        public async Task<List<string>> ReindexerAsync(List<Indices> reindexList, string todayString, string yesterdayString) {
            DateTime timeStamp;
            try
            {
                timeStamp = DateTime.Now;
                Console.WriteLine($"{timeStamp.ToLongTimeString()}:   Getting list of index wildcards to use in filter query....");
                List<string> indexWildcardsUnfiltered = new List<string>();
                //
                for (int l = 0; l < reindexList.Count; l++)
                {
                    string currentIndexWilcardUnfiltered = reindexList.ElementAt(l).Index.ToString();
                    try
                    {
                        var something = reindexList[l].Index.Split('-');
                        string beatName = something[0];
                        string beatVersion = something[1];
                        string indexYear = something[2].Split('.').ElementAt(0);
                        string indexMonth = something[2].Split('.').ElementAt(1);
                        string indexDomain = something[3];
                        string indexTypegroup = something[4];
                        
                        // combine elements to form wildcard string
                        string indexWildcard = beatName + "-" + beatVersion + "-" + indexYear + "." + indexMonth + "*-" + indexDomain + "-" + indexTypegroup;
                        indexWildcardsUnfiltered.Add(indexWildcard);
                    }
                    catch (Exception e)
                    {
                        exceptionList.Add(currentIndexWilcardUnfiltered + "\n" + "Error: " + e.GetBaseException().ToString() + "\n\n");
                        //Console.WriteLine(currentIndexWilcardUnfiltered);
                    }
                }

                if (exceptionList.Count > 0)
                {

                    timeStamp = DateTime.Now;
                    Console.WriteLine($"\n\n{timeStamp.ToLongTimeString()}:   WARNING - Erroneous index names have been found:");
                    for (int err = 0; err < exceptionList.Count; err++)
                    {
                        Console.WriteLine(exceptionList.ElementAt(err));
                    }
                    timeStamp = DateTime.Now;
                    Console.WriteLine($"{timeStamp.ToLongTimeString()}:   Please review these indices after reindexing has occurred.  Possible cause is that on date of index creation, the data stream was written directly to Elasticsearch and did not pass through Logstash mutate filters.");
                    //Console.ReadLine();
                }
                else
                {
                    timeStamp = DateTime.Now;
                    Console.WriteLine($"{timeStamp.ToLongTimeString()}:   All index names adhere to naming convention, proceeding...");
                }

                timeStamp = DateTime.Now;
                Console.WriteLine($"{timeStamp.ToLongTimeString()}:   Filtering out only unique strings....");
                var indexWildcardsUnfilteredArray = indexWildcardsUnfiltered.ToArray();


                List<string> indexWildcards = indexWildcardsUnfilteredArray.Distinct().ToList();

                timeStamp = DateTime.Now;
                Console.WriteLine($"{timeStamp.ToLongTimeString()}:   Wildcard list built.");
                // iterate through wildcard index names, create monthly indices if they don't exist
                for (int w = 0; w < indexWildcards.Count; w++)
                {
                    // get string value of wildcard index
                    string currentWildcard = indexWildcards.ElementAt(w).ToString();
                    
                    // verify that wildcard is not a disallowed index and if so, break
                    if (
                        (
                            currentWildcard.Contains(todayString, StringComparison.OrdinalIgnoreCase) ||
                            currentWildcard.Contains(yesterdayString, StringComparison.OrdinalIgnoreCase)
                        )
                        ||
                        (
                            currentWildcard.Contains("-january", StringComparison.OrdinalIgnoreCase) ||
                            currentWildcard.Contains("-february", StringComparison.OrdinalIgnoreCase) ||
                            currentWildcard.Contains("-march", StringComparison.OrdinalIgnoreCase) ||
                            currentWildcard.Contains("-april", StringComparison.OrdinalIgnoreCase) ||
                            currentWildcard.Contains("-may", StringComparison.OrdinalIgnoreCase) ||
                            currentWildcard.Contains("-june", StringComparison.OrdinalIgnoreCase) ||
                            currentWildcard.Contains("-july", StringComparison.OrdinalIgnoreCase) ||
                            currentWildcard.Contains("-august", StringComparison.OrdinalIgnoreCase) ||
                            currentWildcard.Contains("-september", StringComparison.OrdinalIgnoreCase) ||
                            currentWildcard.Contains("-october", StringComparison.OrdinalIgnoreCase) ||
                            currentWildcard.Contains("-november", StringComparison.OrdinalIgnoreCase) ||
                            currentWildcard.Contains("-december", StringComparison.OrdinalIgnoreCase)
                        )
                    )
                    {
                        timeStamp = DateTime.Now;
                        Console.WriteLine($"{timeStamp.ToLongTimeString()}:   Index {currentWildcard} is currently disallowed based on filters, proceeding to next index....");
                        break;
                    }



                    timeStamp = DateTime.Now;
                    Console.WriteLine($"{timeStamp.ToLongTimeString()}:   Index wildcard match to iterate through and reindex....");
                    Console.WriteLine($"{timeStamp.ToLongTimeString()}:   Wildcard {(w + (int)1).ToString()} of {indexWildcards.Count.ToString()}");
                    Console.WriteLine($"{timeStamp.ToLongTimeString()}:   {currentWildcard}");

                    // set up variables to construct new monthly index name
                    string monthlyIndexName = "";
                    try
                    {
                        string beatName = currentWildcard.Split('-').ElementAt(0);
                        string beatVersion = currentWildcard.Split('-').ElementAt(1);
                        string indexNewDate = currentWildcard.Split('-').ElementAt(2).TrimEnd('*') + ".01";
                        string indexDomain = currentWildcard.Split('-').ElementAt(3);
                        string indexTypegroup = currentWildcard.Split('-').ElementAt(4);

                        string indexMonthName = DateTime.ParseExact(indexNewDate, "yyyy.MM.dd", null).ToString("MMMM").ToLower();

                        monthlyIndexName = beatName + "-" + beatVersion + "-" + indexNewDate + "-" + indexDomain + "-" + indexTypegroup + "-" + indexMonthName;
                    }
                    catch (Exception e)
                    {
                        timeStamp = DateTime.Now;
                        Console.WriteLine($"{timeStamp.ToLongTimeString()}:   \n\nERROR:\n{e.GetBaseException().ToString()}");
                        exceptionList.Add("Exception while tring to build monthly index name" + "Error: " + e.GetBaseException().ToString() + "\n\n");
                    }
                    // check to see if index exists, else create it, then get index object from search with docs.count

                    var result = await GETClient.CheckIndexExists(monthlyIndexName).ConfigureAwait(false);
                    SearchResultsIndexName[] monthlyIndex;
                    SearchResultsIndexName[] currentIndex;

                    if (result.StatusCode != System.Net.HttpStatusCode.OK)
                    {
                        timeStamp = DateTime.Now;
                        Console.WriteLine($"{timeStamp.ToLongTimeString()}:   Monthly index {monthlyIndexName} does not exist, creating....");
                        var createIndex = await PUTClient.CreateNewIndex(monthlyIndexName, jsonIndexSettingsWarm).ConfigureAwait(false);
                        //Thread.Sleep(5000);
                        var monthlyIndexCheck = await GETClient.CheckIndexExists(monthlyIndexName).ConfigureAwait(false);
                        int counterMonthlyIndexCheck = 0;
                        while (monthlyIndexCheck.IsSuccessStatusCode == false)
                        {
                            monthlyIndexCheck = await GETClient.CheckIndexExists(monthlyIndexName).ConfigureAwait(false);
                            counterMonthlyIndexCheck++;
                            if (counterMonthlyIndexCheck >= 5)
                            {
                                break;
                            }
                        }
                    }
                    else
                    {
                        timeStamp = DateTime.Now;
                        Console.WriteLine($"{timeStamp.ToLongTimeString()}:   Index {monthlyIndexName} exists, proceding....");
                    }

                    try
                    {
                        monthlyIndex = (await GETClient.SearchForIndex(monthlyIndexName).ConfigureAwait(false));
                    }
                    catch (Exception e)
                    {
                        timeStamp = DateTime.Now;
                        Console.WriteLine($"{timeStamp.ToLongTimeString()}:   \n\nERROR:\n{e}");
                        exceptionList.Add($"Exception while tring to retrieve index from wildcard search: {monthlyIndexName}" + "\nError: " + e.GetBaseException().ToString() + "\n\n");
                        break;
                    }
                    

                    // get current index object from search to check for status (if status is closed, docs.count will be unavailable)
                    try
                    {
                        SearchResultsIndexName[] searchResults = (await GETClient.SearchForIndices(currentWildcard).ConfigureAwait(false)); //.ToArray();


                        // iterate through search results and process each one
                        for (int sr = 0; sr < searchResults.Length; sr++)
                        {
                            // make sure that current index has correct allocation before proceding
                            try {
                                var currentIndexSettings = await GETClient.GetIndexSettings(searchResults[sr].index).ConfigureAwait(false);
                                //JToken currentIndexSettingsToken = currentIndexSettings.SelectToken("$.Manufacturers[?(@.Name == 'Acme Co')]");

                                if (currentIndexSettings.settings.index.routing.allocation.exclude.box_type.Contains("hot")) // .IndexRouting.IndexAllocation.Exclude.box_type.Contains("hot", StringComparison.OrdinalIgnoreCase))
                                {
                                    Console.WriteLine($"Index is allocated to a an eligible node, proceeding......");
                                }
                                else {
                                    Console.WriteLine($"Index is still allocated to a HOT node and cannot be reindexed into monthly index in this state.");
                                    break;
                                }
                            }
                            catch (Exception e) {
                                timeStamp = DateTime.Now;
                                Console.WriteLine($"{timeStamp.ToLongTimeString()}:   \n\nERROR:\n{e}");
                                exceptionList.Add($"Exception while tring to retrieve index from wildcard search: {searchResults[sr].index}" + "\nError: " + e.GetBaseException().ToString() + "\n\n");
                                break;
                            }

                            // check currently selected index status and get fresh document count from monthly index
                            try
                            {
                                currentIndex = await GETClient.SearchForIndex(searchResults[sr].index).ConfigureAwait(false);
                            }
                            catch (Exception e)
                            {
                                timeStamp = DateTime.Now;
                                Console.WriteLine($"{timeStamp.ToLongTimeString()}:   \n\nERROR:\n{e}");
                                exceptionList.Add($"Exception while tring to retrieve index from wildcard search: {searchResults[sr].index}" + "\nError: " + e.GetBaseException().ToString() + "\n\n");
                                break;
                            }
                            try
                            {
                                monthlyIndex = await GETClient.SearchForIndex(monthlyIndexName).ConfigureAwait(false);
                            }
                            catch (ArgumentNullException nue)
                            {
                                timeStamp = DateTime.Now;
                                List<string> exceptionList = getExceptionList();
                                exceptionList.Add($"{timeStamp.ToLongTimeString()}:   Exception encountered while searching for index using wildcard: {monthlyIndexName}" + "\n" + "Error: " + nue.GetBaseException().ToString() + "\n\n");
                                setExceptionList(exceptionList);
                            }


                            Console.WriteLine($"{timeStamp.ToLongTimeString()}:   Index {(sr + (int)1).ToString()} of {searchResults.Length.ToString()} for wildcard  {(w + (int)1).ToString()} of {indexWildcards.Count.ToString()}");
                            Console.WriteLine($"{timeStamp.ToLongTimeString()}:   {currentIndex[0].index}");

                            // if index is status closed, open it
                            if (!currentIndex[0].status.Contains("open", StringComparison.OrdinalIgnoreCase))
                            {
                                timeStamp = DateTime.Now;
                                Console.ForegroundColor = ConsoleColor.DarkYellow;
                                Console.WriteLine($"{timeStamp.ToLongTimeString()}:   Index {currentIndex[0].index} is closed, setting to open....");
                                Console.ResetColor();
                                var indexOpen = await POSTClient.PostOpenIndex(currentIndex[0].index).ConfigureAwait(false);
                                SearchResultsIndexName[] indexStatusCheck;

                                // check status again and keep looping until index is open waiting 3 seconds in between tries
                                try
                                {
                                    do
                                    {
                                        timeStamp = DateTime.Now;
                                        Console.WriteLine($"{timeStamp.ToLongTimeString()}:   Checking index {currentIndex[0].index} again to see if it is now open....");

                                        indexStatusCheck = await GETClient.SearchForIndex(currentIndex[0].index).ConfigureAwait(false);

                                        Thread.Sleep(3000);
                                    }
                                    while (!indexStatusCheck[0].status.Contains("open", StringComparison.OrdinalIgnoreCase));
                                }
                                catch (ArgumentNullException nue)
                                {
                                    timeStamp = DateTime.Now;
                                    List<string> exceptionList = getExceptionList();
                                    exceptionList.Add($"{timeStamp.ToLongTimeString()}:   Exception encountered while searching for index: {currentIndex[0].index}" + "\n" + "Error: " + nue.GetBaseException().ToString() + "\n\n");
                                    setExceptionList(exceptionList);
                                    Console.ForegroundColor = ConsoleColor.DarkYellow;
                                    Console.WriteLine($"{timeStamp.ToLongTimeString()}:   Checking index {currentIndex[0].index} again to see if it is now open....");
                                    Console.ResetColor();
                                    indexStatusCheck = await GETClient.SearchForIndex(currentIndex[0].index).ConfigureAwait(false);
                                    Thread.Sleep(3000);
                                }

                                timeStamp = DateTime.Now;
                                Console.WriteLine($"{timeStamp.ToLongTimeString()}:   Index {currentIndex[0].index} is now open, proceding....");
                                //try
                                //{
                                //    currentIndex = await GETClient.SearchForIndex(currentIndex[0].index).ConfigureAwait(false);
                                //}
                                //catch (Exception)
                                //{
                                //    Console.WriteLine("failed on 338");
                                //}
                                
                                Console.ForegroundColor = ConsoleColor.DarkCyan;
                                Console.WriteLine($"{timeStamp.ToLongTimeString()}:   Index {currentIndex[0].index} is now open, proceding....ensuring that index is readable and deletable");
                                Console.ResetColor();
                                currentIndex = await GETClient.SearchForIndex(currentIndex[0].index).ConfigureAwait(false);
                                
                                // remove readonly_allow_deletes setting so that index can be processed
                                var removeReadOnly = PUTClient.UpdateSetting(currentIndex[0].index, PUTClient.readOnlyIndices).ConfigureAwait(false);
                            }
                            else
                            {
                                try
                                {
                                    currentIndex = await GETClient.SearchForIndex(currentIndex[0].index).ConfigureAwait(false);
                                }
                                catch (ArgumentNullException nue)
                                {
                                    timeStamp = DateTime.Now;
                                    List<string> exceptionList = getExceptionList();
                                    exceptionList.Add($"{timeStamp.ToLongTimeString()}:   Exception encountered while searching for index using wildcard: {currentIndex[0].index}" + "\n" + "Error: " + nue.GetBaseException().ToString() + "\n\n");
                                    setExceptionList(exceptionList);
                                }
                            }

                            // establish resulting docs count after transfer

                            try
                            {
                                if (!monthlyIndex[0].status.Contains("open", StringComparison.OrdinalIgnoreCase))
                                {
                                    timeStamp = DateTime.Now;
                                    Console.WriteLine($"{timeStamp.ToLongTimeString()}:   Index {monthlyIndex[0].index} is closed, setting to open....");
                                    var indexOpen = await POSTClient.PostOpenIndex(monthlyIndex[0].index).ConfigureAwait(false);
                                    SearchResultsIndexName[] indexMonthlyStatusCheck;

                                    // check status again and keep looping until index is open waiting 3 seconds in between tries
                                    do
                                    {
                                        timeStamp = DateTime.Now;
                                        Console.WriteLine($"{timeStamp.ToLongTimeString()}:   Checking index {monthlyIndex[0].index} again to see if it is now open....");

                                        indexMonthlyStatusCheck = await GETClient.SearchForIndex(monthlyIndex[0].index).ConfigureAwait(false);

                                        Thread.Sleep(3000);
                                    }
                                    while (!indexMonthlyStatusCheck[0].status.Contains("open", StringComparison.OrdinalIgnoreCase));

                                    timeStamp = DateTime.Now;
                                    Console.WriteLine($"{timeStamp.ToLongTimeString()}:   Index {monthlyIndex[0].index} is now open, proceding....");
                                }
                                Thread.Sleep(8000);
                                totalDocsCount = long.Parse(monthlyIndex[0].docsCount) + long.Parse(currentIndex[0].docsCount);
                                string totalDocsCountString = totalDocsCount.ToString();
                                string docsToTransfer = currentIndex[0].docsCount;
                                string existingMonthlyIndexDocsCount = monthlyIndex[0].docsCount;
                            }
                            catch (ArgumentNullException nue) {
                                timeStamp = DateTime.Now;
                                List<string> exceptionList = getExceptionList();
                                exceptionList.Add($"{timeStamp.ToLongTimeString()}:   Argument Null Exception encountered while searching for index: {monthlyIndex[0].index}" + "\n" + "Error: " + nue.GetBaseException().ToString() + "\n\n");
                                setExceptionList(exceptionList);
                                break;
                            }
                            catch (Exception e) {
                                timeStamp = DateTime.Now;
                                List<string> exceptionList = getExceptionList();
                                exceptionList.Add($"{timeStamp.ToLongTimeString()}:   Exception encountered while searching for index: {monthlyIndex[0].index}" + "\n" + "Error: " + e.GetBaseException().ToString() + "\n\n");
                                setExceptionList(exceptionList);
                                break;
                            }

                            timeStamp = DateTime.Now;
                            //Console.WriteLine($"{timeStamp.ToLongTimeString()}:   Merging index { currentIndex[0].index} into monthly index { monthlyIndex[0].index}");
                            //Console.WriteLine($"{timeStamp.ToLongTimeString()}:   Total number of documents to transfer: {currentIndex[0].docsCount}");
                            //Console.WriteLine($"{timeStamp.ToLongTimeString()}:   Total number of existing documents present in monthly index: {monthlyIndex[0].docsCount}");
                            //Console.WriteLine($"{ timeStamp.ToLongTimeString()}:   Final document count after merging should be: {totalDocsCount.ToString()}");
                            Console.Write($"{timeStamp.ToLongTimeString()}:   Merging index ");
                            Console.ForegroundColor = ConsoleColor.DarkCyan;
                            Console.Write($"{currentIndex[0].index}");
                            Console.ResetColor();
                            Console.Write($" into monthly index ");
                            Console.ForegroundColor = ConsoleColor.DarkCyan;
                            Console.Write($"{ monthlyIndex[0].index}\n");
                            Console.ResetColor();

                            Console.Write($"{timeStamp.ToLongTimeString()}:   Total number of documents to transfer: ");
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.Write($"{ currentIndex[0].docsCount}\n");
                            Console.ResetColor();
                            Console.Write($"{timeStamp.ToLongTimeString()}:   Total number of existing documents present in monthly index: ");
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.Write($"{monthlyIndex[0].docsCount}\n");
                            Console.ResetColor();
                            Console.Write($"{ timeStamp.ToLongTimeString()}:   Final document count after merging should be: ");
                            Console.ForegroundColor = ConsoleColor.Cyan;
                            Console.Write($"{totalDocsCount.ToString()}\n");
                            Console.ResetColor();

                            // reindex indices into monthly index e.g.
                            Thread.Sleep(3000);
                            Console.WriteLine($"{timeStamp.ToLongTimeString()}:   Reindexing {currentIndex[0].index} into {monthlyIndex[0].index} now.  Depending on the document count, this may take some time.");
                            long reindexOperation;
                            try
                            {
                                reindexOperation = await POSTClient.PostReindex(currentIndex[0].index, monthlyIndex[0].index, totalDocsCount, GETClient).ConfigureAwait(false);
                            }
                            catch (Exception e)
                            {
                                timeStamp = DateTime.Now;
                                List<string> exceptionList = getExceptionList();
                                exceptionList.Add($"{timeStamp.ToLongTimeString()}:   Exception encountered while searching for reindexing tasks: " + "\n" + "Error: " + e.GetBaseException().ToString() + "\n\n");
                                setExceptionList(exceptionList);
                                break;
                            }

                            if (reindexOperation == totalDocsCount)
                            {
                                timeStamp = DateTime.Now;
                                Console.WriteLine($"{timeStamp.ToLongTimeString()}:   Document copy has completed for index {currentIndex[0].index} into {monthlyIndex[0].index}, checking task status....");
                                try
                                {
                                    var reindexingTasks = await GETClient.GetClusterTasksSimple().ConfigureAwait(false);

                                    if (reindexingTasks.ToString().Contains("reindex") == true)
                                    {
                                        do
                                        {
                                            reindexingTasks = await GETClient.GetClusterTasksSimple().ConfigureAwait(false);
                                            for (int re = 0; re < reindexingTasks.Length; re++)
                                            {
                                                if (reindexingTasks[re].ToString().Contains("reindex") == true)
                                                {
                                                    Console.WriteLine($"{ timeStamp.ToLongTimeString()}:   {reindexingTasks[re].ToString()}");
                                                }
                                            }
                                            Thread.Sleep(15000);
                                        }
                                        while (reindexingTasks.ToString().Contains("reindex") == true);
                                    }
                                }
                                catch (ArgumentNullException nue)
                                {
                                    timeStamp = DateTime.Now;
                                    List<string> exceptionList = getExceptionList();
                                    exceptionList.Add($"{timeStamp.ToLongTimeString()}:   Exception encountered while searching for reindexing tasks: " + "\n" + "Error: " + nue.GetBaseException().ToString() + "\n\n");
                                    setExceptionList(exceptionList);
                                    break;
                                }
                            }

                            try
                            {
                                monthlyIndex = await GETClient.SearchForIndex(monthlyIndexName).ConfigureAwait(false);
                            }
                            catch (ArgumentNullException nue)
                            {
                                timeStamp = DateTime.Now;
                                List<string> exceptionList = getExceptionList();
                                exceptionList.Add($"{timeStamp.ToLongTimeString()}:   Exception encountered while searching for index {monthlyIndexName}: " + "\n" + "Error: " + nue.GetBaseException().ToString() + "\n\n");
                                setExceptionList(exceptionList);
                                break;
                            }


                            timeStamp = DateTime.Now;
                            Console.Write($"{timeStamp.ToLongTimeString()}:   All done with this monthly wildard match to index {monthlyIndexName}, total document count: ");
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.Write($"{monthlyIndex[0].docsCount}\n");
                            Console.ResetColor();
                            processedMonthlyIndices.Add(monthlyIndexName);


                            // when task confirmed complete, delete currentIndex
                            var deleteCurrentIndex = await POSTClient.DeleteIndex(currentIndex[0].index, GETClient).ConfigureAwait(false);
                        }
                    }
                    catch (ArgumentNullException nue)
                    {
                        Console.WriteLine(nue);
                        break;
                    }






                    // GET stats until new index docs count matches the total docs from the indices being reindexed into it

                    // when documents count matches, delete each index that was reindexed
                }


                try
                {
                    return processedMonthlyIndices;
                }
                catch (ArgumentNullException nue)
                {
                    Console.WriteLine(nue);
                    throw;
                }


            }
            catch (IndexOutOfRangeException iore)
            {
                exceptionList.Add("Exception while trying to build index wildcard list and iterate through it for reindexing" + "\n" + "Error: " + iore.GetBaseException().ToString() + "\n\n");
                string processedString = "\n";
                string exceptions = "\n";
                for (int processed = 0; processed < processedMonthlyIndices.Count; processed++)
                {
                    processedString += (processedMonthlyIndices.ElementAt(processed).ToString() + "\n");
                }
                for (int exc = 0; exc < exceptionList.Count; exc++)
                {
                    exceptions += (exceptionList.ElementAt(exc).ToString()+ "\n");
                }
                timeStamp = DateTime.Now;
                Console.WriteLine($"{timeStamp.ToLongTimeString()}:   PROCESSED INDICES:\n" + $"{processedString}\n\nPRINT EXCEPTIONS:\n" + $"{exceptions}");
                return processedMonthlyIndices;
            }
            catch (ArgumentNullException nue)
            {
                Console.WriteLine(nue);
                throw;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        // process next set of listItems
        public void Processor(List<Indices> listItem)
        {

        }
        
    }
}
