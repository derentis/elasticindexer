﻿using ElasticIndexer.SettingsModels;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ElasticIndexer
{
    public class RESTPut
    {
        //private readonly string _name;
        private readonly HttpClient _client;
        private ReindexingProcessor _reindexingProcessor;

        // pass this setting to remove readonly property from index - required before reindexing
        public readonly string readOnlyIndices = "{\"index.blocks.read_only_allow_delete\": null}";

        public Index index { get; set; } = new Index();

        //public Setting(string name, HttpClient client)
        //{
        //    _name = name;
        //    _client = client;
        //}
        public RESTPut(HttpClient client, ReindexingProcessor reindexingProcessor)
        {
            //_name = name;
            _client = client;
            _reindexingProcessor = reindexingProcessor;
        }

        public async Task<bool> UpdateSetting()
        {
            //var result = await _client.PutAsync($"{_name}/_settings", new StringContent(JsonConvert.SerializeObject(this), Encoding.UTF8, "application/json")).ConfigureAwait(false);
            //return result.IsSuccessStatusCode;

            return await Task.Run(() => true).ConfigureAwait(false);
        }

        public async Task<bool> UpdateSetting(string indexName, string jsonObject)
        {
            DateTime timeStamp;
            try
            {
                var result = await _client.PutAsync($"{indexName}/_settings", new StringContent(jsonObject, Encoding.UTF8, "application/json")).ConfigureAwait(false);
                return result.IsSuccessStatusCode;

                //return await Task.Run(() => true).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                timeStamp = DateTime.Now;
                List<string> exceptionList = _reindexingProcessor.getExceptionList();
                exceptionList.Add($"{timeStamp.ToLongTimeString()}:   Exception encountered while updating index settings: {indexName}\n{jsonObject}" + "\n" + "Error: " + e.GetBaseException().ToString() + "\n\n");
                _reindexingProcessor.setExceptionList(exceptionList);
                throw;
            }
        }

        // create new index with settings
        public async Task<bool> CreateNewIndex(string indexName, string jsonObject)
        {
            DateTime timeStamp;
            try
            {
                //JObject stringObject = (JObject)JsonConvert.SerializeObject(jsonObject);
                //var result = await _client.PutAsync($"{indexName}", new StringContent(JsonConvert.SerializeObject(jsonObject), Encoding.UTF8, "application/json")).ConfigureAwait(false);
                var result = await _client.PutAsync($"{indexName}", new StringContent(jsonObject, Encoding.UTF8, "application/json")).ConfigureAwait(false);
                return result.IsSuccessStatusCode;

                //return await Task.Run(() => true).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                timeStamp = DateTime.Now;
                List<string> exceptionList = _reindexingProcessor.getExceptionList();
                exceptionList.Add($"{timeStamp.ToLongTimeString()}:   Exception encountered creating new index: {indexName}\n{jsonObject}" + "\n" + "Error: " + e.GetBaseException().ToString() + "\n\n");
                _reindexingProcessor.setExceptionList(exceptionList);
                throw;
            }
        }

    }
}
