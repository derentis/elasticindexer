﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ElasticIndexer
{
    public class IndexAllocations
    {
        public IndexExclude exclude { get; set; }
        public IndexInclude include { get; set; }
        public IndexRequire require { get; set; }
    }
}
