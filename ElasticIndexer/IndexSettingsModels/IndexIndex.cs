﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ElasticIndexer
{
    public class IndexIndex
    {
        public string codec { get; set; }
        public IndexRoutings routing { get; set; }
        public IndexMappings mapping { get; set; }
        public string refresh_interval { get; set; }
        public string number_of_shards { get; set; }
        public string provided_name { get; set; }
        public string creation_date { get; set; }
        public string number_of_replicas { get; set; }
        public string uuid { get; set; }
        public IndexVersions version { get; set; }
    }
}
