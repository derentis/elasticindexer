﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticIndexer
{
    public class ListBuilder
    {
        // create lists
        private static List<Indices> reindexList = new List<Indices>();
        private static List<Indices> processingList = new List<Indices>();

        // set up dates
        private static DateTime now = DateTime.Now;
        private static readonly string todayString = $"{now.Year}.{now.Month}.{now.Day}";
        private static DateTime yesterday = now.AddDays(-1);
        private static readonly string yesterdayString = $"{yesterday.Year}.{yesterday.Month}.{yesterday.Day}";
        private static readonly DateTime lastWeek = now.AddDays(-7);
        private static readonly DateTime lastMonth = now.AddMonths(-1);

        // Get lists
        public List<Indices> getReindexList()
        {
            return reindexList;
        }

        public List<Indices> getProcessingList()
        {
            return processingList;
        }

        // process master list into two sorted lists
        public static void SeparateLists(Indices[] listItems, RESTGet settingsGetter)
        {
            // build lists
            foreach (var listItem in listItems)
            {
                // filter out currently used indices, closed indices and system indices
                if (
                    (
                        !listItem.Index.Contains(todayString, StringComparison.OrdinalIgnoreCase) ||
                        !listItem.Index.Contains(yesterdayString, StringComparison.OrdinalIgnoreCase)
                    )  &&
                    //String.Equals("open", listItem.Status, StringComparison.OrdinalIgnoreCase) &&
                    (
                        listItem.Index.Contains("auditbeat-", StringComparison.OrdinalIgnoreCase) ||
                        listItem.Index.Contains("metricbeat-", StringComparison.OrdinalIgnoreCase) ||
                        listItem.Index.Contains("winlogbeat-", StringComparison.OrdinalIgnoreCase) ||
                        listItem.Index.Contains("packetbeat-", StringComparison.OrdinalIgnoreCase)
                    )
                    && 
                    (
                        !listItem.Index.Contains("-january", StringComparison.OrdinalIgnoreCase) ||
                        !listItem.Index.Contains("-february", StringComparison.OrdinalIgnoreCase) ||
                        !listItem.Index.Contains("-march", StringComparison.OrdinalIgnoreCase) ||
                        !listItem.Index.Contains("-april", StringComparison.OrdinalIgnoreCase) ||
                        !listItem.Index.Contains("-may", StringComparison.OrdinalIgnoreCase) ||
                        !listItem.Index.Contains("-june", StringComparison.OrdinalIgnoreCase) ||
                        !listItem.Index.Contains("-july", StringComparison.OrdinalIgnoreCase) ||
                        !listItem.Index.Contains("-august", StringComparison.OrdinalIgnoreCase) ||
                        !listItem.Index.Contains("-september", StringComparison.OrdinalIgnoreCase) ||
                        !listItem.Index.Contains("-october", StringComparison.OrdinalIgnoreCase) ||
                        !listItem.Index.Contains("-november", StringComparison.OrdinalIgnoreCase) ||
                        !listItem.Index.Contains("-december", StringComparison.OrdinalIgnoreCase)
                    )
                )
                {

                    // extract and convert date string from index name (zero-based)
                    string indexDateString = listItem.Index.Split('-').ElementAt(2);
                    DateTime indexDateTime = DateTime.ParseExact(indexDateString, "yyyy.MM.dd", null);

                    // if date is from the last week, process onto archival node, else reindex
                    if (indexDateTime.CompareTo(lastWeek) > 0)
                    {
                        //// get selected index settings and check current routing allocation, number of shards, number of replicas
                        //var indexSettings = await settingsGetter.GetIndexSettings(listItem.Index).ConfigureAwait(false);
                        //if (indexSettings.IndexRouting.IndexAllocation.Exclude.box_type == "hot,cold" &&
                        //    indexSettings.IndexRouting.IndexAllocation.Include.box_type.Length == 0 &&
                        //    indexSettings.IndexRouting.IndexAllocation.Require.box_type == "warm"
                        //    )
                        //{
                        //    Console.WriteLine($"{listItem.Index} already has correct allocation");
                        //}
                        //else
                        //{
                        //    processingList.Add(listItem);
                        //    // put box_type settings
                        //    // allocation
                        //    // read-only    
                        //    // forcemerge expunge deletes
                        //}
                    }
                    else
                    {
                        reindexList.Add(listItem);
                        // reindex into monthly
                    }

                }
            }
        }
    }
}
