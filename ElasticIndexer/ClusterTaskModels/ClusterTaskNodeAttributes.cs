﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ElasticIndexer
{
    public class ClusterTaskNodeAttributes
    {
        [JsonProperty("ml.machine_memory")] public string mlMachineMemory { get; set; }
        public string rack { get; set; }
        [JsonProperty("xpack.installed")] public string xpackInstalled { get; set; }
        [JsonProperty("box_type")] public string boxType { get; set; }
        [JsonProperty("ml.max_open_jobs")] public string mlMaxOpenJobs { get; set; }
        [JsonProperty("ml.enabled")] public string mlEnabled { get; set; }
        [JsonProperty("vm_host")] public string vmHost { get; set; }

    }
}
