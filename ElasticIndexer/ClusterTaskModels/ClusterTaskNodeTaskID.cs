﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ElasticIndexer
{
    public class ClusterTaskNodeTaskID
    {
        public string node { get; set; }
        public string id { get; set; }
        public string type { get; set; }
        public string action { get; set; }
        public string description { get; set; }
        [JsonProperty("start_time_in_millis")] public string startTimeInMillis { get; set; }
        [JsonProperty("running_time_in_nanos")] public string runningTimeInNanos { get; set; }
        public string cancellable { get; set; }
        [JsonProperty("parent_task_id")] public string parentTaskId { get; set; }
        [JsonIgnore] public string headers { get; set; }
        [JsonIgnore] public string children { get; set; }
    }
}
