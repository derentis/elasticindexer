﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace ElasticIndexer
{
    public class ClusterTaskNodeProperties
    {
        public string name { get; set; }
        [JsonProperty("transport_address")] public string transportAddress { get; set; }
        public string host { get; set; }
        public string ip { get; set; }
        public Array roles { get; set; }
        public ClusterTaskNodeAttributes attributes { get; set; }
        [SuppressMessage("Microsoft.Performance", "CA1819:PropertiesShouldNotReturnArrays")] public ClusterTaskNodeTasks[] tasks { get; set; }
    }
}
