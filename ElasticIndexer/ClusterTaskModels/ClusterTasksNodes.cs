﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace ElasticIndexer
{
    public class ClusterTasksNodes
    {
        [JsonProperty("HaN11akcSfyXfPfrs3wtcg")] public ClusterTaskNodeProperties rslmon01 { get; set; }          // hot node
        [JsonProperty("2YXJ3aaGTx-00L6ZWkUT3Q")] public ClusterTaskNodeProperties rslmon02 { get; set; }          // hot node
        [JsonProperty("xF-3ifffQS6607MpCTTE1A")] public ClusterTaskNodeProperties rslmon03 { get; set; }          // hot node
        [JsonProperty("RNRfSdTMT4uQgFbGZwNkbA")] public ClusterTaskNodeProperties rslmonarch01 { get; set; }      // warm node
        //[JsonProperty("TBC")] public ClusterTasksNodes[] rslmonarch02 { get; set; }           // cold node
        [JsonProperty("GdeBPMTeQ-WJerJpqx-xew")] public ClusterTaskNodeProperties rslmonk { get; set; }           // coordinator node
        
    }
}
