﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ElasticIndexer.SettingsModels
{
    public class Index
    {
        public Routing routing { get; set; } = new Routing();

        public string number_of_replicas { get; set; } = "0";
        
    }
}
