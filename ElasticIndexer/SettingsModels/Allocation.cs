﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ElasticIndexer.SettingsModels
{
    public class Allocation
    {
        public BoxSettings exclude { get; set; } //= new BoxSettings() { box_type = "hot" };
        public BoxSettings require { get; set; } //= new BoxSettings() { box_type = "warm" };
        public BoxSettings include { get; set; }
    }
}
