﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ElasticIndexer
{
    public class CatTasks
    {
        public string action { get; set; }
        [JsonProperty("task_id")] public string taskId { get; set; }
        [JsonProperty("parent_task_id")] public string parentTaskId { get; set; }
        public string type { get; set; }
        [JsonProperty("start_time")] public string startTime { get; set; }
        public string timestamp { get; set; }
        [JsonProperty("running_time")] public string runningTime { get; set; }
        public string ip { get; set; }
        public string node { get; set; }
    }
}
