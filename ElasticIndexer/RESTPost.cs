﻿using ElasticIndexer.SettingsModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ElasticIndexer
{
    public class RESTPost
    {
        //private readonly string _name;
        private readonly HttpClient _client;
        private ReindexingProcessor _reindexingProcessor;

        public Index index { get; set; } = new Index();

        public RESTPost(HttpClient client, ReindexingProcessor reindexingProcessor)
        {
            _client = client;
            _reindexingProcessor = reindexingProcessor;
        }

        // open an index that has a closed status
        public async Task<HttpResponseMessage> PostOpenIndex(string indexName)
        {
            HttpResponseMessage result = await _client.PostAsync($"{indexName}/_open", new StringContent("", Encoding.UTF8, "application/json")).ConfigureAwait(false);
            return result;

            //return await Task.Run(() => true).ConfigureAwait(false);

        }

        // accepts source index, destination index, final document count that needs to be achieved in destination index for operation to be complete, and RESTGet instance
        // Performs reindexing operation to merge indices
        public async Task<long> PostReindex(string currentIndex, string monthlyIndex, long totalDocsCount, RESTGet GETClient)
        {
            DateTime timeStamp;
            string jsonReindexingString =
            "{" +
                "\"conflicts\": \"proceed\"," +
                "\"source\": {" +
                    $"\"index\": \"{currentIndex}\"" +
                "}," +
                "\"dest\": {" +
                    $"\"index\": \"{monthlyIndex}\"," +
                    "\"op_type\": \"create\"" +
                "}" +
            "}";

            try
            {
                var result = await _client.PostAsync($"_reindex?pretty", new StringContent(jsonReindexingString, Encoding.UTF8, "application/json")).ConfigureAwait(false);

                SearchResultsIndexName[] monthlyIndexObject = await GETClient.SearchForIndex(monthlyIndex).ConfigureAwait(false);
                long docsRemaining = totalDocsCount - long.Parse(monthlyIndexObject[0].docsCount);

                if (long.Parse(monthlyIndexObject[0].docsCount) < totalDocsCount)
                {
                    int docuCountChecker = 0;
                    int failureCounter = 0;
                    string docuNameChecker1 = "";
                    string docuNameChecker2 = "";
                    do
                    {
                        docuCountChecker++;
                        timeStamp = DateTime.Now;
                        //Console.WriteLine($"{timeStamp.ToLongTimeString()}:   Document count of monthly index {monthlyIndexObject[0].index} is {monthlyIndexObject[0].docsCount}, {docsRemaining.ToString()} documents remaining from {currentIndex}...");
                        Console.Write($"{timeStamp.ToLongTimeString()}:   Document count of monthly index {monthlyIndexObject[0].index} is ");
                        Console.ForegroundColor = ConsoleColor.Yellow;
                        Console.Write($"{monthlyIndexObject[0].docsCount}");
                        Console.ResetColor();
                        Console.Write($", ");
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.Write($"{docsRemaining.ToString()} ");
                        Console.ResetColor();
                        Console.Write($"documents remaining from {currentIndex}...\n");
                        Thread.Sleep(15000);
                        monthlyIndexObject = await GETClient.SearchForIndex(monthlyIndex).ConfigureAwait(false);
                        docsRemaining = totalDocsCount - long.Parse(monthlyIndexObject[0].docsCount);
                        Thread.Sleep(15000);
                        monthlyIndexObject = await GETClient.SearchForIndex(monthlyIndex).ConfigureAwait(false);

                        docsRemaining = totalDocsCount - long.Parse(monthlyIndexObject[0].docsCount);

                        // check that index isn't stuck
                        if (docuCountChecker == 1 || docuNameChecker1.Contains(docsRemaining.ToString(), StringComparison.OrdinalIgnoreCase))
                        {
                            docuNameChecker1 = docsRemaining.ToString();
                        }
                        else
                        {
                            docuNameChecker2 = docsRemaining.ToString();
                        }

                        if (docuNameChecker1.Contains(docuNameChecker2, StringComparison.OrdinalIgnoreCase))
                        {
                            failureCounter++;
                        }

                        if (failureCounter >= 10)
                        {
                            timeStamp = DateTime.Now;
                            List<string> exceptionList = _reindexingProcessor.getExceptionList();
                            exceptionList.Add($"{timeStamp.ToLongTimeString()}:   Exception encountered while reindxing {currentIndex} into {monthlyIndex}." + "\n" + "Error: Document count stuck, something went wrong with this index.\n\n");
                            _reindexingProcessor.setExceptionList(exceptionList);
                            throw new Exception();
                        }
                    }
                    while (long.Parse(monthlyIndexObject[0].docsCount) < totalDocsCount);

                    //Console.WriteLine($"{timeStamp.ToLongTimeString()}:   Document count of monthly index {monthlyIndexObject[0].index} is {monthlyIndexObject[0].docsCount}, {docsRemaining.ToString()} documents remaining from {currentIndex}...");
                    Console.Write($"{timeStamp.ToLongTimeString()}:   Document count of monthly index {monthlyIndexObject[0].index} is ");
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.Write($"{monthlyIndexObject[0].docsCount}");
                    Console.ResetColor();
                    Console.Write($", ");
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write($"{docsRemaining.ToString()} ");
                    Console.ResetColor();
                    Console.Write($"documents remaining from {currentIndex}...\n");
                    Thread.Sleep(15000);
                    monthlyIndexObject = await GETClient.SearchForIndex(monthlyIndex).ConfigureAwait(false);
                    docsRemaining = totalDocsCount - long.Parse(monthlyIndexObject[0].docsCount);
                }
                return long.Parse(monthlyIndexObject[0].docsCount);
            }
            catch (TaskCanceledException tce)
            {
                // This operation can take a long time, so this handler catches exception thrown when thread times out then poles for document count which will indicate process progress
                // check document count and write status to console

                SearchResultsIndexName[] monthlyIndexObject = await GETClient.SearchForIndex(monthlyIndex).ConfigureAwait(false);
                long docsRemaining = totalDocsCount - long.Parse(monthlyIndexObject[0].docsCount);
                if (long.Parse(monthlyIndexObject[0].docsCount) < totalDocsCount)
                {
                    int docuCountChecker = 0;
                    int failureCounter = 0;
                    string docuNameChecker1 = "";
                    string docuNameChecker2 = "";
                    do
                    {
                        docuCountChecker++;
                        timeStamp = DateTime.Now;
                        //Console.WriteLine($"{timeStamp.ToLongTimeString()}:   Document count of monthly index {monthlyIndexObject[0].index} is {monthlyIndexObject[0].docsCount}, {docsRemaining.ToString()} documents remaining from {currentIndex}...");
                        Console.Write($"{timeStamp.ToLongTimeString()}:   Document count of monthly index {monthlyIndexObject[0].index} is ");
                        Console.ForegroundColor = ConsoleColor.Yellow;
                        Console.Write($"{monthlyIndexObject[0].docsCount}");
                        Console.ResetColor();
                        Console.Write($", ");
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.Write($"{docsRemaining.ToString()} ");
                        Console.ResetColor();
                        Console.Write($"documents remaining from {currentIndex}...\n");
                        Thread.Sleep(15000);
                        monthlyIndexObject = await GETClient.SearchForIndex(monthlyIndex).ConfigureAwait(false);
                        docsRemaining = totalDocsCount - long.Parse(monthlyIndexObject[0].docsCount);
                        Thread.Sleep(15000);
                        monthlyIndexObject = await GETClient.SearchForIndex(monthlyIndex).ConfigureAwait(false);
                        docsRemaining = totalDocsCount - long.Parse(monthlyIndexObject[0].docsCount);

                        // check that index isn't stuck
                        if (docuCountChecker == 1 || docuNameChecker1.Contains(docsRemaining.ToString(), StringComparison.OrdinalIgnoreCase))
                        {
                            docuNameChecker1 = docsRemaining.ToString();
                        }
                        else
                        {
                            docuNameChecker2 = docsRemaining.ToString();
                        }

                        if (docuNameChecker1.Contains(docuNameChecker2, StringComparison.OrdinalIgnoreCase))
                        {
                            failureCounter++;
                        }

                        if (failureCounter >= 10)
                        {
                            timeStamp = DateTime.Now;
                            List<string> exceptionList = _reindexingProcessor.getExceptionList();
                            exceptionList.Add($"{timeStamp.ToLongTimeString()}:   Exception encountered while reindxing {currentIndex} into {monthlyIndex}." + "\n" + "Error: Document count stuck, something went wrong with this index.\n\n");
                            _reindexingProcessor.setExceptionList(exceptionList);
                            throw new Exception();
                        }
                    }
                    while (long.Parse(monthlyIndexObject[0].docsCount) < totalDocsCount);

                    //Console.WriteLine($"{timeStamp.ToLongTimeString()}:   Document count of monthly index {monthlyIndexObject[0].index} is {monthlyIndexObject[0].docsCount}, {docsRemaining.ToString()} documents remaining from {currentIndex}...");
                    Console.Write($"{timeStamp.ToLongTimeString()}:   Document count of monthly index {monthlyIndexObject[0].index} is ");
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.Write($"{monthlyIndexObject[0].docsCount}");
                    Console.ResetColor();
                    Console.Write($", ");
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write($"{docsRemaining.ToString()} ");
                    Console.ResetColor();
                    Console.Write($"documents remaining from {currentIndex}...\n");
                    Thread.Sleep(15000);
                    monthlyIndexObject = await GETClient.SearchForIndex(monthlyIndex).ConfigureAwait(false);
                    docsRemaining = totalDocsCount - long.Parse(monthlyIndexObject[0].docsCount);
                }
                return long.Parse(monthlyIndexObject[0].docsCount);
            }
            catch (ArgumentNullException nue)
            {
                timeStamp = DateTime.Now;
                List<string> exceptionList = _reindexingProcessor.getExceptionList();
                exceptionList.Add($"{timeStamp.ToLongTimeString()}:   Exception encountered while searching for indices while reindexing {currentIndex} into {monthlyIndex}" + "\n" + "Error: " + nue.GetBaseException().ToString() + "\n\n");
                _reindexingProcessor.setExceptionList(exceptionList);
                throw;
            }
            catch (Exception e)
            {
                timeStamp = DateTime.Now;
                List<string> exceptionList = _reindexingProcessor.getExceptionList();
                exceptionList.Add($"{timeStamp.ToLongTimeString()}:   Exception encountered while reindxing {currentIndex} into {monthlyIndex}." + "\n" + "Error: " + e.GetBaseException().ToString() + "\n\n");
                _reindexingProcessor.setExceptionList(exceptionList);
                throw;
            }
            

            //return await Task.Run(() => true).ConfigureAwait(false);

        }

        public async Task<Boolean> DeleteIndex(string indexName, RESTGet GETClient)
        {
            DateTime timeStamp;
            try
            {
                timeStamp = DateTime.Now;
                Console.ForegroundColor = ConsoleColor.Magenta;
                Console.WriteLine($"{timeStamp.ToLongTimeString()}:   Deleting index {indexName}");
                Console.ResetColor();
                var result = await _client.DeleteAsync($"{indexName}").ConfigureAwait(false);
                if (result.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    throw new Exception();
                }
                return true;
            }
            catch (Exception e)
            {
                // in case delete operation takes a while, query tasks for delete status
                try
                {
                    var runningTasks = await GETClient.GetClusterTasksSimple().ConfigureAwait(false);
                    if (runningTasks.ToString().Contains("delete") == true)
                    {
                        do
                        {
                            runningTasks = await GETClient.GetClusterTasksSimple().ConfigureAwait(false);
                            for (int rt = 0; rt < runningTasks.Length; rt++)
                            {
                                if (runningTasks[rt].ToString().Contains("reindex") == true)
                                {
                                    Console.WriteLine(runningTasks[rt].ToString());
                                }
                            }
                            Thread.Sleep(15000);
                        }
                        while (runningTasks.ToString().Contains("delete") == true);
                    }
                    return true;
                }
                catch (Exception)
                {
                    timeStamp = DateTime.Now;
                    List<string> exceptionList = _reindexingProcessor.getExceptionList();
                    exceptionList.Add($"{timeStamp.ToLongTimeString()}:   Exception encountered while deleting index: {indexName}" + "\n" + "Error: " + e.GetBaseException().ToString() + "\n\n");
                    _reindexingProcessor.setExceptionList(exceptionList);
                    throw;
                }
            }
        }
    }
}
