﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using System.Threading.Tasks;

namespace ElasticIndexer
{
    public static class Program
    {
        public static async Task Main(string[] args)
        {
            DateTime now = DateTime.Now;
            string todayString = $"{now.Year}.{now.Month}.{now.Day}";
            DateTime yesterday = now.AddDays(-1);
            string yesterdayString = $"{yesterday.Year}.{yesterday.Month}.{yesterday.Day}";
            DateTime lastWeek = now.AddDays(-7);
            DateTime lastMonth = now.AddMonths(-1);
            DateTime timeStamp;

            var handler = new HttpClientHandler();
            handler.ClientCertificateOptions = ClientCertificateOption.Manual;
            handler.SslProtocols = SslProtocols.Tls12;

            handler.ServerCertificateCustomValidationCallback = (httpRequestMessage, cert, cetChain, policyErrors) =>
            {
                return true;
            };

            handler.ClientCertificates.Add(new X509Certificate2("newbundlewithkey.pfx"));
            var client = new HttpClient(handler);

            client.BaseAddress = new Uri("https://rslmon01.internal.ramm.com:9200");

            var GETClient = new RESTGet(client);

            var listItemsReturned = await GETClient.GetIndices().ConfigureAwait(false);
            var listItems = listItemsReturned.ToArray();

            var recordCount = listItems.Count();
            timeStamp = DateTime.Now;
            Console.WriteLine($"{timeStamp.ToLongTimeString()}:   Processing {recordCount} records...");

            // build lists
            ListBuilder.SeparateLists(listItems, GETClient);
            ListBuilder listBuilder = new ListBuilder();
            List<Indices> reindexList = listBuilder.getReindexList();

            try
            {
                if (reindexList.Count > (int)0)
                {
                    ReindexingProcessor reindexer = new ReindexingProcessor(client);
                    var reindexingOp = await reindexer.ReindexerAsync(reindexList, todayString, yesterdayString).ConfigureAwait(false);
                }
            }
            catch (ArgumentNullException ane)
            {
                timeStamp = DateTime.Now;
                Console.WriteLine($"{timeStamp.ToLongTimeString()}:   ArgumentNullException encountered base" + "\n" + "Error: " + ane.GetBaseException().ToString() + "\n\n");
                
            }
            catch (Exception e)
            {
                timeStamp = DateTime.Now;
                Console.WriteLine($"{timeStamp.ToLongTimeString()}:   Exception encountered" + "\n" + "Error: " + e.GetBaseException().ToString() + "\n\n");
                
            }
        }
    }
}
