﻿using Core;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ElasticIndexer
{
    public class RESTGet
    {
        private HttpClient _client;
        private ReindexingProcessor _reindexingProcessor;
        readonly JsonSerializerSettings jsonSettings = new JsonSerializerSettings
        
        {
            NullValueHandling = NullValueHandling.Ignore,
            MissingMemberHandling = MissingMemberHandling.Ignore
        };


        public RESTGet(HttpClient client)
        {
            _client = client;
        }

        public RESTGet(HttpClient client, ReindexingProcessor reindexingProcessor)
        {
            _client = client;
            _reindexingProcessor = reindexingProcessor;
        }

        

        // get list of indices
        public async Task<IEnumerable<Indices>> GetIndices()
        {
            DateTime timeStamp;
            try
            {
            timeStamp = DateTime.Now;
            Console.WriteLine($"{timeStamp.ToLongTimeString()}:   Getting indices....");
                var result = await _client.GetStringAsync("_cat/indices?format=json").ConfigureAwait(false);

                return JsonConvert.DeserializeObject<Indices[]>(result, jsonSettings);
            }
            catch (Exception e)
            {
                //List<string> exceptionList = _reindexingProcessor.getExceptionList();
                //exceptionList.Add("Exception encountered while getting indices" + "\n" + "Error: " + e.GetBaseException().ToString() + "\n\n");
                //_reindexingProcessor.setExceptionList(exceptionList);
                throw;
            }
        }

        // get individual index settings
        public async Task<IndexConfiguration> GetIndexSettings(string currentIndex)
        {
            DateTime timeStamp;
            try
            {
                timeStamp = DateTime.Now;
                Console.WriteLine($"{timeStamp.ToLongTimeString()}:   Getting document count for index {currentIndex}");
                var result = await _client.GetStringAsync($"{currentIndex}/_settings?format=json").ConfigureAwait(false);

                var jsonObject = (JObject)JsonConvert.DeserializeObject(result, jsonSettings);
                var jsonToken = jsonObject.GetValue(currentIndex).ToObject<IndexConfiguration>();

                return jsonToken;

                //timeStamp = DateTime.Now;
                //Console.WriteLine($"{timeStamp.ToLongTimeString()}:   Getting index settings for {currentIndex}");
                //var result = await _client.GetStringAsync($"{currentIndex}/_settings?format=json").ConfigureAwait(false);
                ////var settings = JsonConvert.DeserializeObject<IndexSettings[]>(result);
                ////var settings2 = JsonConvert.DeserializeObject<IndexSettings[]>(result, jsonSettings);
                ////var set2 = JsonConvert.DeserializeObject<IndexSettings>(result, jsonSettings);
                //var token = (JObject)JsonConvert.DeserializeObject(result, jsonSettings);
                ////var token2 = JsonConvert.DeserializeObject<IndexConfiguration>(result, jsonSettings);
                ////var tekon = JsonConvert.DeserializeObject(result, jsonSettings);
                //var obje = JObject.Parse(result);
                //var returner = JsonConvert.DeserializeObject<IndexSettings>(result, jsonSettings);
                //var indexObject = JsonConvert.DeserializeObject<IndexConfiguration>(result, jsonSettings).settings;
                //var jt = JToken.Parse(result);
                //return obje;
                //var selected = jt.SelectToken($"{currentIndex}");
                //var settings 4 = token.GetValue()
                //return settings;
                //var stats = token.
                //.GetValue(currentIndex).ToObject<IndexStats>()?._all.primaries.docs;
            }
            catch (Exception e)
            {
                timeStamp = DateTime.Now;
                List<string> exceptionList = _reindexingProcessor.getExceptionList();
                exceptionList.Add($"{timeStamp.ToLongTimeString()}:   Exception encountered while getting index settings: {currentIndex}" + "\n" + "Error: " + e.GetBaseException().ToString() + "\n\n");
                _reindexingProcessor.setExceptionList(exceptionList);
                throw;
            }
        }

        // get individual index stats - docs count, deleted count
        public async Task<IndexStatsDocs> GetIndexStatsDocs(string currentIndex)
        {
            DateTime timeStamp;
            try
            {
                timeStamp = DateTime.Now;
                Console.WriteLine($"{timeStamp.ToLongTimeString()}:   Getting document count for index {currentIndex}");
                var result = await _client.GetStringAsync($"{currentIndex}/_stats?format=json").ConfigureAwait(false);
                var stats = JsonConvert.DeserializeObject<IndexStatsDocs>(result, jsonSettings);
                //var stats = token.
                //.GetValue(currentIndex).ToObject<IndexStats>()?._all.primaries.docs;

                return stats;
            }
            catch (Exception e)
            {
                timeStamp = DateTime.Now;
                List<string> exceptionList = _reindexingProcessor.getExceptionList();
                exceptionList.Add($"{timeStamp.ToLongTimeString()}:   Exception encountered while getting index stats: {currentIndex}" + "\n" + "Error: " + e.GetBaseException().ToString() + "\n\n");
                _reindexingProcessor.setExceptionList(exceptionList);
                throw;
            }
        }

        // check if index exists
        public async Task<HttpResponseMessage> CheckIndexExists(string currentIndex)
        {
            DateTime timeStamp;
            try
            {
                timeStamp = DateTime.Now;
                Console.WriteLine($"{timeStamp.ToLongTimeString()}:   Checking whether or not index {currentIndex} exists...");
                HttpResponseMessage result = await _client.GetAsync($"{currentIndex}").ConfigureAwait(false);

                return result;
            }
            catch (Exception e)
            {
                timeStamp = DateTime.Now;
                List<string> exceptionList = _reindexingProcessor.getExceptionList();
                exceptionList.Add($"{timeStamp.ToLongTimeString()}:   Exception encountered while checking if index exists: {currentIndex}" + "\n" + "Error: " + e.GetBaseException().ToString() + "\n\n");
                _reindexingProcessor.setExceptionList(exceptionList);
                throw;
            }
        }

        // search cluster for wildcard indices
        public async Task<SearchResultsIndexName[]> SearchForIndices(string wildcardIndexString)
        {
            DateTime timeStamp;
            try
            {
                timeStamp = DateTime.Now;
                Console.WriteLine($"{timeStamp.ToLongTimeString()}:   Searching for indices containing the following wildcard match: {wildcardIndexString}");
                var result = await _client.GetStringAsync($"/_cat/indices/{wildcardIndexString}?format=json").ConfigureAwait(false);
                try
                {
                    return JsonConvert.DeserializeObject<SearchResultsIndexName[]>(result);
                }
                catch (Exception e)
                {
                    timeStamp = DateTime.Now;
                    List<string> exceptionList = _reindexingProcessor.getExceptionList();
                    exceptionList.Add($"{timeStamp.ToLongTimeString()}:   Exception encountered while searching for indices with wildcard {wildcardIndexString}" + "\n" + "Error: " + e.GetBaseException().ToString() + "\n\n");
                    _reindexingProcessor.setExceptionList(exceptionList);
                    throw;
                }
                
            }
            catch (HttpRequestException hre)
            {
                timeStamp = DateTime.Now;
                List<string> exceptionList = _reindexingProcessor.getExceptionList();
                exceptionList.Add($"{timeStamp.ToLongTimeString()}:   Exception encountered while searching for index using wildcard string:\n{wildcardIndexString}" + "\n" + "Error: " + hre.GetBaseException().ToString() + "\n\n");
                _reindexingProcessor.setExceptionList(exceptionList);
                throw;
            }
            catch (Exception e)
            {
                timeStamp = DateTime.Now;
                List<string> exceptionList = _reindexingProcessor.getExceptionList();
                exceptionList.Add($"{timeStamp.ToLongTimeString()}:   Exception encountered while searching for index using wildcard string:\n{wildcardIndexString}" + "\n" + "Error: " + e.GetBaseException().ToString() + "\n\n");
                _reindexingProcessor.setExceptionList(exceptionList);
                throw;
            }
            
        }

        // search cluster for single index
        public async Task<SearchResultsIndexName[]> SearchForIndex(string wildcardIndexString)
        {
            DateTime timeStamp;
            try
            {
                timeStamp = DateTime.Now;
                Console.WriteLine($"{timeStamp.ToLongTimeString()}:   Searching for single index containing the following wildcard match: {wildcardIndexString}");
                var result = await _client.GetStringAsync($"/_cat/indices/{wildcardIndexString}?format=json").ConfigureAwait(false);
                //var token = (JObject[])JsonConvert.DeserializeObject(result);
                //var searchResults = token.GetValue(0).ToString().toOb  .ToObject<SearchResultsIndexName>();

                return JsonConvert.DeserializeObject<SearchResultsIndexName[]>(result);  //JsonConvert.DeserializeObject<SearchResultsIndexName>(result);
            }
            catch (HttpRequestException hre)
            {
                timeStamp = DateTime.Now;
                List<string> exceptionList = _reindexingProcessor.getExceptionList();
                exceptionList.Add($"{timeStamp.ToLongTimeString()}:   Exception encountered while searching for index using wildcard: {wildcardIndexString}" + "\n" + "Error: " + hre.GetBaseException().ToString() + "\n\n");
                _reindexingProcessor.setExceptionList(exceptionList);
                throw;
            }

            catch (ArgumentNullException nue)
            {
                timeStamp = DateTime.Now;
                List<string> exceptionList = _reindexingProcessor.getExceptionList();
                exceptionList.Add($"{timeStamp.ToLongTimeString()}:   Exception encountered while searching for index using wildcard: {wildcardIndexString}" + "\n" + "Error: " + nue.GetBaseException().ToString() + "\n\n");
                _reindexingProcessor.setExceptionList(exceptionList);
                throw;
            }
            catch (Exception e)
            {
                timeStamp = DateTime.Now;
                List<string> exceptionList = _reindexingProcessor.getExceptionList();
                exceptionList.Add($"{timeStamp.ToLongTimeString()}:   Exception encountered while searching for index using wildcard: {wildcardIndexString}" + "\n" + "Error: " + e.GetBaseException().ToString() + "\n\n");
                _reindexingProcessor.setExceptionList(exceptionList);
                throw;
            }
        }

        // check status of single task action type
        public async Task<CatTasks[]> GetClusterTasksSimple()
        {
            DateTime timeStamp;
            try
            {
                timeStamp = DateTime.Now;
                Console.WriteLine($"{timeStamp.ToLongTimeString()}:   Searching for any relevant running tasks....");
                var result = await _client.GetStringAsync("_cat/tasks?format=json").ConfigureAwait(false);

                return JsonConvert.DeserializeObject<CatTasks[]>(result, jsonSettings);
            }
            catch (Exception  e)
            {
                timeStamp = DateTime.Now;
                List<string> exceptionList = _reindexingProcessor.getExceptionList();
                exceptionList.Add($"{timeStamp.ToLongTimeString()}:   Exception encountered while getting cluster tasks" + "\n" + "Error: " + e.GetBaseException().ToString() + "\n\n");
                _reindexingProcessor.setExceptionList(exceptionList);
                throw;
            }

        }
    }
}
